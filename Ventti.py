# random generaattorin todentaminen
import random
# Hello World teksin tekeminen ja tulostaminen tarkistaakseen ohjelman toimivuus
msg = "Hello World"
print(msg)
# Kortti pakan tekeminen, arvot ja arvojen määrä kertaa 4
pakka = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]*4

# Kahden ensimmäisen kortin jakaminen
def jako(pakka):
    # Tyhjä taulukko
    kasi = []
    # For lauseessa kahden kortin ottaminen
    for i in range(2):
        random.shuffle(pakka)
        kortti = pakka.pop()
        #Kortti on arvoltaan 14 eli ässä
        if kortti == 14:
            kortti == 'A'
        kasi.append(kortti)
    return kasi
# Pisteiden lasku
def pisteet(kasi):
    pisteet = 0
    # Kortit kädessä
    for kortti in kasi:
        # Jos kortti on ässä ja pisteitä on vähemmän kuin 8 on ässä arvo 14, muuten 1
        if kortti == 'A':
            if pisteet >= 8:
                pisteet += 1
            else:
                pisteet += 14
        else:
            pisteet += kortti
    # Palautetaan pisteet
    return pisteet
# Lisäkorttien nosto kahden ensimmäisen jälkeen, jos ei ole tyytyväinen pistemääräänsä
def kortinnosto(kasi):
    kortti = pakka.pop()
    if kortti == 14:
        kortti == 'A'
    kasi.append(kortti)
    return kasi
# Pelaajan ja jakajan kortit ja niiden arvot erikseen ja yhteensä
def arvot(jakajankasi, pelaajankasi):
    print("Pelaajan kortin arvo", pelaajankasi, "kokonaisarvo", pisteet(pelaajankasi))
    print("Jakajan kortin arvo", jakajankasi, "kokonaisarvo", pisteet(jakajankasi))
# Tulokset pisteiden vertaamiseen lopussa
def tulos(jakajankasi, pelaajankasi):
    if pisteet(pelaajankasi) < pisteet(jakajankasi):
        print("Hävisit, jakajalla on paremmat pisteet")
    elif pisteet(pelaajankasi) > pisteet(jakajankasi):
        print("Voitit, sinulla on paremmat pisteet kuin jakajalla")
# Tulokset, jos pistemäärä menee yli sallitun rajan tai saa ventin 21 pisteellä
def valiaikatulos(jakajankasi, pelaajankasi):
    if pisteet(jakajankasi) > 21:
        print("Jakajan pisteet meni yli 21, voitit")
        exit()
    if pisteet(pelaajankasi) > 21:
        print("Pisteesi meni yli 21, hävisit")
        exit()
    if pisteet(pelaajankasi) == 21:
        print("Sait ventin")
        exit()
    if pisteet(jakajankasi) == 21:
        print("Jakaja sai ventin")
        exit()
# Ohjeet pelin alussa ja heti sen jälkeen jaetaan kortit ja katsotaan tuloksia
valinta = 0
print("Pelaat venttiä")
print("Pelissä sinulle ja jakajalle jaetaan 2 korttia, joiden arvot lasketaan yhteen")
print("Voitat pelin jos saat 21 pistettä tai jakajan pistemäärä menee yli 21 tai sinulla on enemmän pisteitä kuin jakajalla\n")
print("Valinnalla 1 pelaaja nostaa itselleen kortteja niin kauan, kuin on pisteihinsä tyytyväinen")
print("Valinnalla 2 nostetaan jakajalle kortteja niin kauan, että jakajalla olisi paremmat pisteet kuin pelaajalla")
print("Valinnalla 3 peli lopetetaan")
print("Valinnalla 4 julistetaan lopputulos\n")
# Pelaajan ensimmäiset 2 korttia
pelaajankasi = jako(pakka)
# Jakajan ensimmäiset 2 korttia
jakajankasi = jako(pakka)
# Pisteet
arvot(jakajankasi, pelaajankasi)
# Tarkastetaan saatiinko venttiä vai menikö jo pistemäärä yli
valiaikatulos(jakajankasi, pelaajankasi)
while valinta != 3:
    # Ohjeiden ja arvojen perusteella tehdään valinnat sen jälkeen
    valinta=input("Tee valinta, 1 tai 2 tai 3 tai 4: ")
    # Nostetaan pelaajalle kortteja
    if valinta == "1":
        kortinnosto(pelaajankasi)       
        arvot(jakajankasi, pelaajankasi)
        valiaikatulos(jakajankasi, pelaajankasi)
    # Nostetaan jakajalle kortteja
    elif valinta == "2":       
        kortinnosto(jakajankasi)
        arvot(jakajankasi, pelaajankasi)
        valiaikatulos(jakajankasi, pelaajankasi)
    # Lopetetaan peli
    elif valinta == "3":
        break
    # Kun jakaja on tyytyväinen pisteisiinsä annetaan lopulliset tulokset
    elif valinta == "4":
        tulos(jakajankasi, pelaajankasi)
        break
    else:
        print("\nVirhe, virheellinen syöte, syötä oikealla välillä oleva numero\n")
        
